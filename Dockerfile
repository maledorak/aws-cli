FROM alpine:3.10
ARG AWS_CLI_VERSION=1.16.251
RUN apk -v --update add \
        python3 \
        py3-pip \
        groff \
        less \
        mailcap \
        && \
    pip3 install --upgrade awscli==${AWS_CLI_VERSION} s3cmd==2.0.2 python-magic && \
    apk -v --purge del py-pip && \
    rm /var/cache/apk/*
VOLUME /root/.aws
VOLUME /project
ADD docker-entrypoint.sh /root/docker-entrypoint.sh
WORKDIR /project
ENTRYPOINT ["/root/docker-entrypoint.sh"]
